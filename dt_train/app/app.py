"""
This is the implementation of training for rossman store sales prediction
"""

__author__ = "Naveen Sinha"

import pickle
import pandas as pd
import numpy as np
import os
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.model_selection import RandomizedSearchCV
from xgboost.sklearn import XGBRegressor
import scipy.stats as st


class Training:
    def __init__(self, base_folder="/data"):

        self.base_folder = base_folder
        self.combined_train_data = None
        self.features_train = None
        self.features_test = None
        self.labels_train = None
        self.labels_test = None

    def load(self):
        self.combined_train_data = pd.read_csv(
            os.path.join(self.base_folder,
                         "combined_train.csv"))

        # create X and y
        combined_train_data1 = self.combined_train_data.sample(frac=0.2)
        print(combined_train_data1.head())
        feature_cols = ['CompetitionDistance', 'Promo', 'Promo2',
                        'NewAssortment', 'NewStoreType']
        X = combined_train_data1[feature_cols]
        y = combined_train_data1.Sales
        y1 = combined_train_data1.Customers
        self.features_train, self.features_test, self.labels_train, self.labels_test = train_test_split(
            X, y, test_size=0.3, random_state=42)
        for col in feature_cols:
            print(self.features_train[col].head())
            if np.isnan(self.features_train[col]).any():
                self.features_train.fillna(0, inplace=True)
            if np.isnan(self.features_test[col]).any():
                self.features_test.fillna(0, inplace=True)
            print(np.isfinite(self.features_train).all())
            print(np.isfinite(self.features_test).all())
        print(self.labels_train.head())
        print(np.isfinite(self.labels_train).all())

    def train(self):
        print("Training decision tree regressor")
        print(self.features_train.head())

        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)

        params = {
            "n_estimators": st.randint(3, 40),
            "max_depth": st.randint(3, 40),
            "learning_rate": st.uniform(0.05, 0.4),
            "colsample_bytree": one_to_left,
            "subsample": one_to_left,
            "gamma": st.uniform(0, 10),
            "reg_alpha": from_zero_positive,
            "min_child_weight": from_zero_positive,
        }
        mdl = XGBRegressor()
        model = RandomizedSearchCV(mdl, params, n_jobs=1)

        print("Start training")
        model.fit(self.features_train, self.labels_train)
        print("Training Completed")
        print("Calculating accuracy")
        print(self.features_test.shape)
        r2_score_train = r2_score(self.labels_train,
                                  model.predict(self.features_train))
        r2_score_test = r2_score(self.labels_test,
                                 model.predict(self.features_test))
        print("R2Score Train:{} R2Score Test: {}".format(r2_score_train,
                                                         r2_score_test))
        with open("/data/xgboost.pkl", "wb") as model_fs:
            pickle.dump(model, model_fs)


if __name__ == "__main__":
    trainer = Training(base_folder="/data")
    trainer.load()
    trainer.train()
