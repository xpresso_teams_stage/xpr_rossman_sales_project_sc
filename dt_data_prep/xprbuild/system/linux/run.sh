#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application
export PYTHONPATH=${ROOT_FOLDER}:${PYTHONPATH}
echo "172.16.3.2 controllerqa.xpresso.ai" >> /etc/hosts
cat /etc/hosts
echo "Starting the service"
python3 ${ROOT_FOLDER}/app/xpresso_app.py $@
