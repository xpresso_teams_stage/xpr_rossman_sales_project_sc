"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
from json import JSONDecodeError

__author__ = "Naveen Sinha"

from flask import Flask, request, jsonify
import os
import pickle
import pandas as pd

config_file = 'config/dev.json'


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


class Inference:
    def __init__(self, base_folder="/data"):
        self.base_folder = base_folder
        with open(os.path.join(base_folder, "xgboost.pkl"),
                  "rb") as model_fs:
            self.model = pickle.load(model_fs)
        self.feature_cols = ['CompetitionDistance', 'Promo', 'Promo2',
                             'NewAssortment', 'NewStoreType']

    @staticmethod
    def run():
        app.run(host="0.0.0.0")

    def predict(self, data):
        feature = pd.DataFrame()
        if isinstance(data, list):
            feature = pd.DataFrame([data], columns=self.feature_cols)
        elif isinstance(data, dict):
            try:
                data_to_list = [data[col] for col in self.feature_cols]
                feature = pd.DataFrame(data=data,
                                       columns=self.feature_cols)
            except (ValueError, TypeError, IndexError):
                return None
        else:
            return None
        result = self.model.predict(feature)
        print(list(result))
        return result.tolist()


inference = Inference()


@app.route('/predict', methods=["POST"])
def request_predict():
    """
        Take predict request and send response back to user
        """
    try:
        input_json = request.get_json()
    except(ValueError, IndexError, JSONDecodeError):
        return {"message": "Invalid JSON"}, 400

    if 'data' not in input_json:
        return {"message": "Data not provided"}, 400
    output = inference.predict(data=input_json['data'])
    if not output:
        return {"message": "Invalid data provided"}, 400
    return jsonify({"message": "success", "results": output}), 200


if __name__ == '__main__':
    inference.run()
