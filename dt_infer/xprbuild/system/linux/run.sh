#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application
export PYTHONPATH=${ROOT_FOLDER}:${PYTHONPATH}
env
echo "Starting the service"
sleep 20
python3 ${ROOT_FOLDER}/app/xpresso_app.py $@
cat /.xpr/logs/default.log
